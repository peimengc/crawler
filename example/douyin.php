<?php

use GuzzleHttp\Cookie\CookieJar;
use Peimengc\Crawler\Douyin;
use Peimengc\Crawler\Exception\ResponseException;

require __DIR__ . '/../vendor/autoload.php';

const COOKIE_FILE = __DIR__ . '/cookie.json';

$cookieArr = json_decode(file_get_contents(COOKIE_FILE), 1);

$cookieJar = $cookieArr ? new CookieJar(false, $cookieArr) : login();

$douyin = new Douyin($cookieJar);

//通知-评论列表
//$noticeResult = $douyin->notice();
//print_r($noticeResult);

//个人信息
//$userInfo = $douyin->creator()->userInfo();

//视频信息
//$itemlistResult = $douyin->creator()->itemList();
//视频id
//$item_id = $itemlistResult['item_info_list'][0]['item_id'];
//评论列表
//$commentListResult = $douyin->creator()->commentList($item_id);
//评论id
//$commentId = $commentListResult['comment_info_list'][0]['comment_id'];
//删除评论
//$commentDeleteResult = $douyin->creator()->commentDelete($commentId);
//print_r($commentDeleteResult);


//评论回复列表
//$commentId = '@i/5r5L9KQ0/9sM5G6Lnuu8x5pSiQZS/xJqwrO6xqgZghuhhBretTIFVE4ZxMpRC3';
//$replyListResult = $douyin->creator()->replyList($commentId);
//print_r($replyListResult);


function login()
{
    $cookieJar = new CookieJar();

    $douyin = new Douyin($cookieJar);

    try {
        $result = $douyin->getQrcode();

        $token = $result['data']['token'];

        $qrcode = $result['data']['qrcode'];

        print_r($qrcode . "\r\n");

        sleep(10);

        do {
            $result = $douyin->checkQrconnect($token);
            if ($result['data']['status'] == 3) {
                file_put_contents(COOKIE_FILE, json_encode($cookieJar->toArray()));
                return $cookieJar;
            }

            sleep(2);
        } while (true);

    } catch (ResponseException $e) {
        print_r($e->raw);
        print_r($e->getTrace());
    }
    return null;
}

