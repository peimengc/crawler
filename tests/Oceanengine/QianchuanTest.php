<?php

namespace Oceanengine;

use Peimengc\Crawler\Exception\ResponseException;
use Peimengc\Crawler\Qianchuan;
use PHPUnit\Framework\TestCase;

class QianchuanTest extends TestCase
{
    public function testRequest()
    {
        $response = ['code' => 0, 'message' => 'OK'];
        $q = \Mockery::mock(Qianchuan::class, ['mock-appid', 'mock-secret'])->makePartial();
        $q->shouldReceive('preRequest')
            ->with('GET', 'https://baidu.com', [
                'headers' => [
                    'Access-Token' => '',
                    'Content-Type' => 'application/json'
                ],
                'base_uri' => $q->baseUri
            ])
            ->andReturn($response);

        $result = $q->request('GET', 'https://baidu.com');

        $this->assertSame($response, $result);

        $errorResponse = ['code' => 40002, 'message' => 'PERMISSION_ERROR'];
        $q = \Mockery::mock(Qianchuan::class, ['mock-appid', 'mock-secret'])->makePartial();
        $q->shouldReceive('preRequest')
            ->with('GET', 'https://baidu.com', [
                'headers' => [
                    'Access-Token' => '',
                    'Content-Type' => 'application/json'
                ],
                'base_uri' => $q->baseUri
            ])
            ->andReturn($errorResponse);

        $this->expectException(ResponseException::class);

        $q->request('GET', 'https://baidu.com');

    }

    public function testOauth2AccessToken()
    {
        $code = 'mock-code';
        $appid = 'mock-appid';
        $secret = 'mock-secret';
        $result = [
            'code' => 0,
            'message' => 'success',
            'data' => [
                'access_token' => '123456',
                'expires_in' => 7200,
                'refresh_token' => '123456',
                'refresh_token_expires_in' => 7200,
            ]
        ];

        $q = \Mockery::mock(Qianchuan::class, [$appid, $secret])->makePartial();

        $q->shouldReceive('request')
            ->with('GET', '/oauth2/access_token/', [
                'query' => [
                    'app_id' => $appid,
                    'secret' => $secret,
                    'grant_type' => 'auth_code',
                    'auth_code' => $code
                ]
            ])
            ->andReturn($result);

        $this->assertSame($result, $q->oauth2AccessToken($code));
    }

    public function testOauth2RefreshToken()
    {
        $refreshToken = 'mock-refresh-token';
        $appid = 'mock-appid';
        $secret = 'mock-secret';
        $result = [
            'code' => 0,
            'message' => 'success',
            'data' => [
                'access_token' => '123456',
                'expires_in' => 7200,
                'refresh_token' => '123456',
                'refresh_token_expires_in' => 7200,
            ]
        ];

        $q = \Mockery::mock(Qianchuan::class, [$appid, $secret])->makePartial();

        $q->shouldReceive('request')
            ->with('GET', '/oauth2/refresh_token/', [
                'query' => [
                    'app_id' => $appid,
                    'secret' => $secret,
                    'grant_type' => 'refresh_token',
                    'refresh_token' => $refreshToken
                ]
            ])
            ->andReturn($result);

        $this->assertSame($result, $q->oauth2RefreshToken($refreshToken));
    }
}