<?php

use Peimengc\Crawler\Utils;
use PHPUnit\Framework\TestCase;

class UtilsTest extends TestCase
{
    public function testFormParams2Multipart()
    {
        $params = [
            'advertiser_id' => 123,
            'upload_type' => 'UPLOAD_BY_FILE',
            'image_signature' => 'aaa',
            'image_file' => 'aaa',
            'filename' => 'aaa'
        ];

        $params = Utils::formParams2Multipart($params);

        $this->assertSame($params, [
            [
                'name' => 'advertiser_id',
                'contents' => 123,
            ], [
                'name' => 'upload_type',
                'contents' => 'UPLOAD_BY_FILE',
            ], [
                'name' => 'image_signature',
                'contents' => 'aaa',
            ], [
                'name' => 'image_file',
                'contents' => 'aaa',
            ], [
                'name' => 'filename',
                'contents' => 'aaa',
            ]
        ]);
    }
}