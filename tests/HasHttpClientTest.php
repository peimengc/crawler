<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Peimengc\Crawler\Http\HasHttpClient;
use PHPUnit\Framework\TestCase;

class HasHttpClientTest extends TestCase
{

    public function testGetHttpClient()
    {
        $c = Mockery::mock(HasHttpClient::class);
        $this->assertInstanceOf(Client::class, $c->getHttpClient());
    }

    public function testRequest()
    {
        $response = new Response('200', [], '{}');

        $client = Mockery::mock(Client::class);
        $client->shouldReceive('request')
            ->with('GET', 'https://baidu.com/test', [])
            ->andReturn($response);

        $c = Mockery::mock(HasHttpClient::class)->makePartial();
        $c->httpClient = $client;
        $this->assertSame($response, $c->request('GET', 'https://baidu.com/test'));
    }

    public function testGet()
    {
        $response = new Response('200', [], '{}');

        $client = Mockery::mock(Client::class);
        $client->shouldReceive('request')
            ->with('GET', 'https://baidu.com/test', [
                'query' => [
                    'aaa' => 1
                ]
            ])
            ->andReturn($response);

        $c = Mockery::mock(HasHttpClient::class)->makePartial();
        $c->httpClient = $client;
        $this->assertSame($response, $c->get('https://baidu.com/test', [
            'aaa' => 1
        ]));
    }

    public function testPost()
    {
        $response = new Response('200', [], '{}');

        $client = Mockery::mock(Client::class);
        $client->shouldReceive('request')
            ->with('POST', 'https://baidu.com/test', [
                'query' => [
                    'bbb' => 2
                ],
                'form_params' => [
                    'aaa' => 1
                ],
            ])
            ->andReturn($response);

        $c = Mockery::mock(HasHttpClient::class)->makePartial();
        $c->httpClient = $client;
        $this->assertSame($response, $c->post('https://baidu.com/test', [
            'aaa' => 1
        ], [
            'bbb' => 2
        ]));

    }

    public function testPostJson()
    {
        $response = new Response('200', [], '{}');

        $client = Mockery::mock(Client::class);
        $client->shouldReceive('request')
            ->with('POST', 'https://baidu.com/test', [
                'query' => [
                    'bbb' => 2
                ],
                'json' => [
                    'aaa' => 1
                ],
            ])
            ->andReturn($response);

        $c = Mockery::mock(HasHttpClient::class)->makePartial();
        $c->httpClient = $client;
        $this->assertSame($response, $c->postJson('https://baidu.com/test', [
            'aaa' => 1
        ], [
            'bbb' => 2
        ]));

    }
}