<?php

namespace Peimengc\Crawler;

use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\GuzzleException;
use Peimengc\Crawler\Douyin\Creator;
use Peimengc\Crawler\Douyin\Sso;
use Peimengc\Crawler\Douyin\Ttwid;
use Peimengc\Crawler\Exception\AuthorizationException;
use Peimengc\Crawler\Exception\ResponseException;
use Peimengc\Crawler\Http\HasHttpClient;

class Douyin
{
    use HasHttpClient {
        request as preRequest;
    }

    public CookieJar|null $cookieJar = null;

    public string $baseUri = 'https://www.douyin.com';

    public string $format = 'array';

    protected Creator|null $creator = null;

    protected Sso|null $sso = null;

    protected Ttwid|null $ttwid = null;

    public function __construct(CookieJar $cookieJar = null)
    {
        $this->cookieJar = $cookieJar;
    }

    /**
     * @return Creator
     */
    public function creator(): Creator
    {
        if (!$this->creator) {
            $this->creator = new Creator($this->cookieJar);
        }

        return $this->creator;
    }


    public function ttwid()
    {
        if (!$this->ttwid) {
            $this->ttwid = new Ttwid();
        }

        return $this->ttwid;
    }

    /**
     * @return Sso
     */
    public function sso(): Sso
    {
        if (!$this->sso) {
            $this->sso = new Sso($this->cookieJar);
        }

        return $this->sso;
    }

    /**
     * 发送请求
     * @param string $method
     * @param string $url
     * @param array $options
     * @return array
     */
    public function request(string $method, string $url, array $options = []): array
    {
        $options['cookies'] = $this->cookieJar;
        $options['base_uri'] = rtrim($this->baseUri, '/') . '/';
        $url = ltrim($url, '/');

        return $this->preRequest($method, $url, $options);
    }

    /**
     * 获取抖音网页通知-评论
     *
     * @param int $minTime
     * @param int $maxTime
     * @return array
     * @throws AuthorizationException
     */
    public function notice(int $minTime = 0, int $maxTime = 0): array
    {
        return $this->get('/aweme/v1/web/notice/', [
            'device_platform' => 'webapp',
            'aid' => '6383',
            'channel' => 'channel_pc_web',
            'is_new_notice' => '1',
            'is_mark_read' => '1',
            'notice_group' => '2',
            'count' => '10',
            'min_time' => $minTime,
            'max_time' => $maxTime,
            'pc_client_type' => '1',
            'version_code' => '170400',
            'version_name' => '17.4.0',
            'cookie_enabled' => 'true',
            'screen_width' => '1920',
            'screen_height' => '1080',
            'browser_language' => 'zh-CN',
            'browser_platform' => 'Win32',
            'browser_name' => 'Edge',
            'browser_version' => '114.0.1823.67',
            'browser_online' => 'true',
            'engine_name' => 'Blink',
            'engine_version' => '114.0.0.0',
            'os_name' => 'Windows',
            'os_version' => '10',
            'cpu_core_num' => '6',
            'device_memory' => '8',
            'platform' => 'PC',
            'downlink' => '10',
            'effective_type' => '4g',
            'round_trip_time' => '50',
            'webid' => '7250062750687233536',
            'msToken' => '-ysBfv-vwcdrKb7OBPdYuQAUSGRr05MslyX9bKAdoErGdxhR9wU6tqnVYMsTRRwlSkexwgjNzjtT0CGv-2QJLTq0M_R3TS6FW6JbjaDltnQ55pHnXt6aDQ==',
            'X-Bogus' => 'DFSzswVLUxXANClptJtOXsoB6ls7',
        ]);
    }

    /**
     * 获取登录二维码
     *
     * @return array
     * @throws AuthorizationException
     */
    public function getQrcode(): array
    {
        //获取 ttwid cookie
        $ttwid = $this->ttwid()->unionRegister();
        $ticket = Utils::getUrlQuery($ttwid['redirect_url'], 'ticket');
        $this->creator()->unionRegisterCallback($ticket);
        return $this->sso()->getQrcode();
    }

    /**
     * 检测扫码状态
     *
     * @param string $token
     * @return array
     * @throws AuthorizationException
     */
    public function checkQrconnect(string $token): array
    {
        $result = $this->sso()->checkQrconnect($token);

        if ($result['data']['status'] == 3) {
            $ticket = Utils::getUrlQuery($result['data']['redirect_url'], 'ticket');
            $this->creator()->ssoLoginCallback($ticket);
        }

        return $result;
    }
}
