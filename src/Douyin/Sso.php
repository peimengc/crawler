<?php

namespace Peimengc\Crawler\Douyin;

use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\GuzzleException;
use Peimengc\Crawler\Http\HasHttpClient;

class Sso
{
    use HasHttpClient {
        request as preRequest;
    }

    public CookieJar|null $cookieJar = null;

    public string $baseUri = 'https://sso.douyin.com';

    public string $format = 'array';

    public function __construct(CookieJar $cookieJar = null)
    {
        $this->cookieJar = $cookieJar;
    }

    /**
     * 发送请求
     *
     * @param string $method
     * @param string $url
     * @param array $options
     * @return array
     * @throws GuzzleException
     */
    public function request(string $method, string $url, array $options = []): array
    {
        $options['cookies'] = $this->cookieJar;
        $options['base_uri'] = $this->baseUri;
        $options['headers'] = [
            'authority' => 'sso.douyin.com',
            'accept' => 'application/json, text/plain, */*',
            'accept-language' => 'zh-CN,zh;q=0.9',
            'origin' => 'https://creator.douyin.com',
            'referer' => 'https://creator.douyin.com/',
            'sec-ch-ua' => '"Not.A/Brand";v="8", "Chromium";v="114", "Microsoft Edge";v="114"',
            'sec-ch-ua-mobile' => '?0',
            'sec-ch-ua-platform' => '"Windows"',
            'sec-fetch-dest' => 'empty',
            'sec-fetch-mode' => 'cors',
            'sec-fetch-site' => 'same-site',
            'user-agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36 Edg/114.0.1823.67',
        ];

        return $this->preRequest($method, $url, $options);
    }


    /**
     * 获取二维码
     * @return array
     * @throws GuzzleException
     */
    public function getQrcode(): array
    {
        return $this->get('/get_qrcode/', [
            'next' => 'https://creator.douyin.com/creator-micro/home',
            'service' => 'https://creator.douyin.com',
            'is_vcd' => '1',
            'aid' => '2906',
        ]);
    }

    /**
     * 检测token
     *
     * @param string $token
     * @return array
     * @throws GuzzleException
     */
    public function checkQrconnect(string $token): array
    {
        return $this->get('/check_qrconnect/', [
            'next' => 'https://creator.douyin.com/creator-micro/home',
            'token' => $token,
            'service' => 'https://creator.douyin.com/?logintype=undefined&loginapp=undefined&jump=https://creator.douyin.com/creator-micro/home',
            'correct_service' => 'https://creator.douyin.com/?logintype=undefined&loginapp=undefined&jump=https://creator.douyin.com/creator-micro/home',
            'aid' => '2906',
            'is_vcd' => '1',
        ]);
    }
}
