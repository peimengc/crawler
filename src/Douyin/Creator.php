<?php

namespace Peimengc\Crawler\Douyin;

use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\GuzzleException;
use Peimengc\Crawler\Http\HasHttpClient;

class Creator
{
    use HasHttpClient {
        request as preRequest;
    }

    public CookieJar|null $cookieJar = null;

    public string $baseUri = 'https://creator.douyin.com';

    public string $format = 'array';

    public function __construct(CookieJar $cookieJar = null)
    {
        $this->cookieJar = $cookieJar;
    }

    /**
     * 发送请求
     *
     * @param string $method
     * @param string $url
     * @param array $options
     * @return array
     * @throws GuzzleException
     */
    public function request(string $method, string $url, array $options = []): array
    {
        $options['cookies'] = $this->cookieJar;
        $options['base_uri'] = $this->baseUri;

        return $this->preRequest($method, $url, $options);
    }

    /**
     * 用户信息
     *
     * @throws GuzzleException
     */
    public function userInfo(): array
    {
        return $this->get('/web/api/media/user/info/');
    }

    /**
     * 视频列表
     *
     * @param int $cursor
     * @param int $count
     * @return array
     * @throws GuzzleException
     */
    public function itemList(int $cursor = 0, int $count = 10): array
    {
        return $this->get('/aweme/v1/creator/item/list/', [
            'cursor' => $cursor,
            'count' => $count,
            'aid' => '2906',
            'app_name' => 'aweme_creator_platform',
            'device_platform' => 'web',
            'referer' => 'https://creator.douyin.com/',
            'user_agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36 Edg/114.0.1823.67',
            'cookie_enabled' => 'true',
            'screen_width' => '1920',
            'screen_height' => '1080',
            'browser_language' => 'zh-CN',
            'browser_platform' => 'Win32',
            'browser_name' => 'Mozilla',
            'browser_version' => '5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36 Edg/114.0.1823.67',
            'browser_online' => 'true',
            'timezone_name' => 'Asia/Shanghai',
        ]);
    }

    /**
     * 视频评论列表
     *
     * @param string $item_id
     * @param int $cursor
     * @param int $count
     * @return array
     * @throws GuzzleException
     */
    public function commentList(string $item_id, int $cursor = 0, int $count = 10): array
    {
        return $this->get('/aweme/v1/creator/comment/list/', [
            'item_id' => $item_id,
            'count' => $count,
            'cursor' => $cursor,
            'sort' => 'TIME',
            'aid' => '2906',
            'app_name' => 'aweme_creator_platform',
            'device_platform' => 'web',
            'referer' => 'https://creator.douyin.com/',
            'user_agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36 Edg/114.0.1823.67',
            'cookie_enabled' => 'true',
            'screen_width' => '1920',
            'screen_height' => '1080',
            'browser_language' => 'zh-CN',
            'browser_platform' => 'Win32',
            'browser_name' => 'Mozilla',
            'browser_version' => '5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36 Edg/114.0.1823.67',
            'browser_online' => 'true',
            'timezone_name' => 'Asia/Shanghai',
        ]);
    }

    /**
     * 评论回复列表
     *
     * @param string $comment_id
     * @param int $cursor
     * @param int $count
     * @return array
     * @throws GuzzleException
     */
    public function replyList(string $comment_id, int $cursor = 0, int $count = 10): array
    {
        return $this->get('/aweme/v1/creator/comment/reply/list/', [
            'comment_id' => $comment_id,
            'count' => $count,
            'cursor' => $cursor,
            'sort' => 'TIME',
            'aid' => '2906',
            'app_name' => 'aweme_creator_platform',
            'device_platform' => 'web',
            'referer' => 'https://creator.douyin.com/',
            'user_agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36 Edg/114.0.1823.67',
            'cookie_enabled' => 'true',
            'screen_width' => '1920',
            'screen_height' => '1080',
            'browser_language' => 'zh-CN',
            'browser_platform' => 'Win32',
            'browser_name' => 'Mozilla',
            'browser_version' => '5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36 Edg/114.0.1823.67',
            'browser_online' => 'true',
            'timezone_name' => 'Asia/Shanghai',
        ]);
    }

    /**
     * 删除评论
     *
     * @param string $commentId
     * @return array
     * @throws GuzzleException
     */
    public function commentDelete(string $commentId): array
    {
        return $this->postJson('/aweme/v1/creator/comment/delete/', [
            'comment_Id' => $commentId,
            'aid' => '2906',
            'app_name' => 'aweme_creator_platform',
            'device_platform' => 'web',
            'referer' => 'https://creator.douyin.com/',
            'user_agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36 Edg/114.0.1823.67',
            'cookie_enabled' => 'true',
            'screen_width' => '1920',
            'screen_height' => '1080',
            'browser_language' => 'zh-CN',
            'browser_platform' => 'Win32',
            'browser_name' => 'Mozilla',
            'browser_version' => '5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36 Edg/114.0.1823.67',
            'browser_online' => 'true',
            'timezone_name' => 'Asia/Shanghai',
        ]);
    }

    /**
     * 获取ttwid cookie
     *
     * @param $ticket
     * @return array
     * @throws GuzzleException
     */
    public function unionRegisterCallback($ticket): array
    {
        return $this->get('/ttwid/union/register/callback/', [
            'aid' => '2906',
            'ticket' => $ticket
        ]);
    }

    /**
     * 登录回调,获取cookie
     *
     * @param string $ticket
     * @return array
     * @throws GuzzleException
     */
    public function ssoLoginCallback(string $ticket): array
    {
        return $this->preRequest('GET', '/passport/sso/login/callback/', [
            'cookies' => $this->cookieJar,
            'base_uri' => $this->baseUri,
            'query' => [
                "next" => "https://creator.douyin.com/?logintype=undefined&loginapp=undefined&jump=https://creator.douyin.com/creator-micro/home",
                "ticket" => $ticket
            ]
        ]);
    }
}
