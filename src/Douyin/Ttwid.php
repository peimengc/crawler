<?php

namespace Peimengc\Crawler\Douyin;

use GuzzleHttp\Exception\GuzzleException;
use Peimengc\Crawler\Http\HasHttpClient;

class Ttwid
{
    use HasHttpClient {
        request as preRequest;
    }

    public string $format = 'array';

    /**
     * 发送请求
     *
     * @param string $method
     * @param string $url
     * @param array $options
     * @return array
     * @throws GuzzleException
     */
    public function request(string $method, string $url, array $options = []): array
    {
        $options['headers']['referer'] = 'https://creator.douyin.com/';

        return $this->preRequest($method, $url, $options);
    }

    /**
     * @return array
     * @throws GuzzleException
     */
    public function unionRegister(): array
    {
        return $this->postJson(
            'https://ttwid.bytedance.com/ttwid/union/register/',
            [
                "aid" => 2906,
                "service" => "creator.douyin.com",
                "region" => "cn",
                "needFid" => false,
                "union" => true,
                "fid" => ""
            ]
        );
    }
}
