<?php

namespace Peimengc\Crawler;

use GuzzleHttp\Exception\GuzzleException;
use Peimengc\Crawler\Exception\ResponseException;
use Peimengc\Crawler\Http\HasHttpClient;

class Qianchuan
{
    use HasHttpClient {
        request as preRequest;
    }

    public string $accessToken = '';

    public string $baseUri = 'https://ad.oceanengine.com/open_api/';

    public string $appId;

    public string $secret;

    public string $format = 'array';

    public function __construct($app_id, $secret)
    {
        $this->appId = $app_id;
        $this->secret = $secret;
    }

    /**
     * 发送请求
     *
     * @param string $method
     * @param string $url
     * @param array $options
     * @return array
     * @throws GuzzleException
     * @throws ResponseException
     */
    public function request(string $method, string $url, array $options = []): array
    {
        $options['headers']['Access-Token'] = $this->accessToken;
        $options['base_uri'] = rtrim($this->baseUri, '/') . '/';
        $url = ltrim($url, '/');

        $result = $this->preRequest($method, $url, $options);

        if ($result['code'] !== 0) {
            throw new ResponseException($result);
        }

        return $result;
    }

    /**
     * 获取Token
     *
     * @param string $authCode
     * @return array
     * @throws GuzzleException
     */
    public function oauth2AccessToken(string $authCode): array
    {
        $result = $this->postJson('/oauth2/access_token/', [
            'app_id' => $this->appId,
            'secret' => $this->secret,
            'grant_type' => 'auth_code',
            'auth_code' => $authCode,
        ]);

        if ($result['code'] === 0) {
            $this->accessToken = $result['data']['access_token'];
        }

        return $result;
    }

    /**
     * 刷新Token
     *
     * @param string $refreshToken
     * @return array
     * @throws GuzzleException
     */
    public function oauth2RefreshToken(string $refreshToken): array
    {
        $result = $this->postJson('/oauth2/refresh_token/', [
            'app_id' => $this->appId,
            'secret' => $this->secret,
            'grant_type' => 'refresh_token',
            'refresh_token' => $refreshToken,
        ]);

        if ($result['code'] === 0) {
            $this->accessToken = $result['data']['access_token'];
        }

        return $result;
    }

    /**
     * 获取千川账户下已授权抖音号
     *
     * @param array $param
     * @return array
     * @throws GuzzleException
     */
    public function awemeAuthorizedGet(array $param): array
    {
        return $this->get('/v1.0/qianchuan/aweme/authorized/get/', $param);
    }

    /**
     * 获取已授权的账户（店铺/代理商）
     *
     * @return array
     * @throws GuzzleException
     */
    public function oauth2AdvertiserGet(): array
    {
        return $this->get('/oauth2/advertiser/get/', [
            'app_id' => $this->appId,
            'secret' => $this->secret,
            'access_token' => $this->accessToken
        ]);
    }

    /**
     * 获取店铺账户关联的广告账户列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function shopAdvertiserList(array $params): array
    {
        return $this->get('/v1.0/qianchuan/shop/advertiser/list/', $params);
    }

    /**
     * 获取代理商账户关联的广告账户列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function agentAdvertiserSelect(array $params): array
    {
        return $this->get('/2/agent/advertiser/select/', $params);
    }

    /**
     * 获取授权时登录用户信息
     *
     * @return array
     * @throws GuzzleException
     */
    public function userInfo(): array
    {
        return $this->get('/2/user/info/');
    }

    /**
     * 获取店铺账户信息
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function shopGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/shop/get/', $params);
    }

    /**
     * 获取代理商账户信息
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function agentInfo(array $params): array
    {
        return $this->get('/2/agent/info/', $params);
    }

    /**
     * 获取千川广告账户基础信息
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function advertiserPublicInfo(array $params): array
    {
        return $this->get('/2/advertiser/public_info/', $params);
    }

    /**
     * 获取千川广告账户全量信息
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function advertiserInfo(array $params): array
    {
        return $this->get('/2/advertiser/info/', $params);
    }

    /**
     * 获取千川账户类型
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function advertiserTypeGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/advertiser/type/get/', $params);
    }

    /**
     * 获取账户钱包信息
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function financeWalletGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/finance/wallet/get/', $params);
    }

    /**
     * 获取财务流水信息
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function financeDetailGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/finance/detail/get/', $params);
    }

    /**
     * 广告组创建
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function campaignCreate(array $params): array
    {
        return $this->postJson('/v1.0/qianchuan/campaign/create/', $params);
    }

    /**
     * 广告组更新
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function campaignUpdate(array $params): array
    {
        return $this->postJson('/v1.0/qianchuan/campaign/update/', $params);
    }

    /**
     * 广告组状态更新
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function batchCampaignStatusUpdate(array $params): array
    {
        return $this->postJson('/v1.0/qianchuan/batch_campaign_status/update/', $params);
    }

    /**
     * 广告组列表获取
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function campaignListGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/campaign_list/get/', $params);
    }

    /**
     * 创建计划（含创意生成规则）
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function adCreate(array $params): array
    {
        return $this->postJson('/v1.0/qianchuan/ad/create/', $params);
    }

    /**
     * 更新计划（含创意生成规则）
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function adUpdate(array $params): array
    {
        return $this->postJson('/v1.0/qianchuan/ad/update/', $params);
    }

    /**
     * 更新计划状态
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function adStatusUpdatee(array $params): array
    {
        return $this->postJson('/v1.0/qianchuan/ad/status/update/', $params);
    }

    /**
     * 更新计划预算
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function adBudgetUpdatee(array $params): array
    {
        return $this->postJson('/v1.0/qianchuan/ad/budget/update/', $params);
    }

    /**
     * 更新计划出价
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function adBidUpdatee(array $params): array
    {
        return $this->postJson('/v1.0/qianchuan/ad/bid/update/', $params);
    }

    /**
     * 获取计划详情（含创意信息）
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function adDetailGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/ad/detail/get/', $params);
    }

    /**
     * 获取账户下计划列表（不含创意）
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function adGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/ad/get/', $params);
    }

    /**
     * 获取计划审核建议
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function adRejectReason(array $params): array
    {
        return $this->get('/v1.0/qianchuan/ad/reject_reason/', $params);
    }

    /**
     * 获取低效计划列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function lqAdGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/lq_ad/get/', $params);
    }

    /**
     * 获取支付ROI目标建议
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function suggestRoiGoal(array $params): array
    {
        return $this->get('/v1.0/qianchuan/suggest/roi/goal', $params);
    }

    /**
     * 获取非ROI目标建议出价
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function suggestBid(array $params): array
    {
        return $this->get('/v1.0/qianchuan/suggest_bid/', $params);
    }

    /**
     * 更新计划的支付ROI目标
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function roiGoalUpdate(array $params): array
    {
        return $this->postJson('/v1.0/qianchuan/roi/goal/update', $params);
    }

    /**
     * 更新创意状态
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function creativeStatusUpdate(array $params): array
    {
        return $this->postJson('/v1.0/qianchuan/creative/status/update/', $params);
    }

    /**
     * 获取账户下创意列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function creativeGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/creative/get/', $params);
    }

    /**
     * 获取创意审核建议
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function creativeRejectReason(array $params): array
    {
        return $this->get('/v1.0/qianchuan/creative/reject_reason/', $params);
    }

    /**
     * 商家获取可投商品列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function productAvailableGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/product/available/get/', $params);
    }

    /**
     * 达人获取可投商品列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function awemeProductAvailableGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/aweme/product/available/get/', $params);
    }

    /**
     * 获取计划的搜索关键词
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function adKeywordsGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/ad/keywords/get/', $params);
    }

    /**
     * 更新关键词
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function adKeywordsUpdate(array $params): array
    {
        return $this->postJson('/v1.0/qianchuan/ad/keywords/update/', $params);
    }

    /**
     * 获取系统推荐的搜索关键词
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function adRecommendKeywordsGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/ad/recommend_keywords/get', $params);
    }

    /**
     * 关键词合规校验
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function keywordCheck(array $params): array
    {
        return $this->postJson('/v1.0/qianchuan/keyword/check/', $params);
    }

    /**
     * 获取否定词列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function adPivativewordsGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/ad/pivativewords/get/', $params);
    }

    /**
     * 全量更新否定词
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function adPivativewordsUpdate(array $params): array
    {
        return $this->postJson('/v1.0/qianchuan/ad/pivativewords/update/', $params);
    }

    /**
     * 获取广告账户数据
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function reportAdvertiserGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/report/advertiser/get/', $params);
    }

    /**
     * 获取广告计划数据
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function reportAdGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/report/ad/get/', $params);
    }

    /**
     * 获取广告创意数据
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function reportCreativeGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/report/creative/get/', $params);
    }

    /**
     * 获取广告素材数据
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function reportMaterialGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/report/material/get/', $params);
    }

    /**
     * 获取搜索词/关键词数据
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function reportSearchWordGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/report/search_word/get/', $params);
    }

    /**
     * 视频互动流失数据
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function reportVideoUserLoseGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/report/video_user_lose/get/', $params);
    }

    /**
     * 长周期转化价值-订单明细
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function reportLongTransferOrderGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/report/long_transfer/order/get/', $params);
    }

    /**
     * 获取今日直播数据
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function reportLiveGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/report/live/get/', $params);
    }

    /**
     * 获取今日直播间列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function todayLiveRoomGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/today_live/room/get/', $params);
    }

    /**
     * 获取直播间详情
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function todayLiveRoomDetailGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/today_live/room/detail/get/', $params);
    }

    /**
     * 获取直播间流量表现
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function todayLiveRoomFlowPerformanceGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/today_live/room/flow_performance/get/', $params);
    }

    /**
     * 获取直播间用户洞察
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function todayLiveRoomUserGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/today_live/room/user/get/', $params);
    }

    /**
     * 获取直播间商品列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function todayLiveRoomProductListGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/today_live/room/product_list/get/', $params);
    }

    /**
     * 获取商品竞争分析列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function productAnalyseList(array $params): array
    {
        return $this->get('/v1.0/qianchuan/product/analyse/list/', $params);
    }

    /**
     * 商品竞争分析详情-效果对比
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function productAnalyseCompareStatsData(array $params): array
    {
        return $this->get('/v1.0/qianchuan/product/analyse/compare_stats_data/', $params);
    }

    /**
     * 商品竞争分析详情-创意比对
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function productAnalyseCompareCreative(array $params): array
    {
        return $this->get('/v1.0/qianchuan/product/analyse/compare_creative/', $params);
    }


    /**
     * 创建随心推订单
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function awemeOrderCreate(array $params): array
    {
        return $this->postJson('/v1.0/qianchuan/aweme/order/create/', $params);
    }

    /**
     * 终止随心推订单
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function awemeOrderTerminate(array $params): array
    {
        return $this->postJson('/v1.0/qianchuan/aweme/order/terminate/', $params);
    }

    /**
     * 获取随心推订单列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function awemeOrderGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/aweme/order/get/', $params);
    }

    /**
     * 获取随心推订单详情
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function awemeOrderDetailGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/aweme/order/detail/get/', $params);
    }

    /**
     * 获取随心推订单数据
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function awemeReportOrderGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/aweme/report/order/get/', $params);
    }

    /**
     * 获取随心推兴趣标签
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function awemeInterestActionInterestKeyword(array $params): array
    {
        return $this->get('/v1.0/qianchuan/aweme/interest_action/interest/keyword/', $params);
    }

    /**
     * 获取随心推可投视频列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function awemeVideoGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/aweme/video/get/', $params);
    }

    /**
     * 获取随心推投放效果预估
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function awemeEstimateProfit(array $params): array
    {
        return $this->get('/v1.0/qianchuan/aweme/estimate_profit/', $params);
    }

    /**
     * 获取随心推短视频建议出价
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function awemeSuggestBid(array $params): array
    {
        return $this->get('/v1.0/qianchuan/aweme/suggest_bid/', $params);
    }

    /**
     * 获取随心推ROI建议出价
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function awemeSuggestRoiGoal(array $params): array
    {
        return $this->get('/v1.0/qianchuan/aweme/suggest/roi/goal/', $params);
    }

    /**
     * 查询随心推使用中订单配额信息
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function awemeOrderQuotaGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/aweme/order/quota/get/', $params);
    }

    /**
     * 上传图片素材
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function fileImageAd(array $params): array
    {
        return $this->fileUpload('/2/file/image/ad/', Utils::formParams2Multipart($params));
    }

    /**
     * 上传视频素材
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function fileVideoAd(array $params): array
    {
        return $this->fileUpload('/2/file/video/ad/', Utils::formParams2Multipart($params));
    }

    /**
     * 获取抖音号下的视频
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function fileVideoAwemeGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/file/video/aweme/get/', $params);
    }

    /**
     * 获取千川素材库图片
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function imageGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/image/get/', $params);
    }

    /**
     * 获取千川素材库视频
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function videoGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/video/get/', $params);
    }

    /**
     * 获取首发素材
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function fileVideoOriginalGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/file/video/original/get/', $params);
    }

    /**
     * 获取低效素材
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function fileVideoEfficiencyGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/file/video/efficiency/get/', $params);
    }

    /**
     * 批量删除图片素材
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function fileImageDelete(array $params): array
    {
        return $this->get('/v1.0/qianchuan/file/image/delete/', $params);
    }

    /**
     * 批量删除视频素材
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function fileVideoDelete(array $params): array
    {
        return $this->get('/v1.0/qianchuan/file/video/delete/', $params);
    }

    /**
     * 获取行业列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolsIndustryGet(array $params): array
    {
        return $this->get('/2/tools/industry/get/', $params);
    }

    /**
     * 操作日志查询
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolsLogSearch(array $params): array
    {
        return $this->get('/2/tools/log_search/', $params);
    }

    /**
     * 获取定向受众预估
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolsEstimateAudience(array $params): array
    {
        return $this->get('/v1.0/qianchuan/tools/estimate_audience/', $params);
    }

    /**
     * 获取在投计划配额信息
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function adQuotaGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/ad/quota/get/', $params);
    }

    /**
     * 查询抖音类目下的推荐达人
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolsAwemeCategoryTopAuthorGet(array $params): array
    {
        return $this->get('/2/tools/aweme_category_top_author/get/', $params);
    }

    /**
     * 查询抖音类目列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolsAwemeMultiLevelCategoryGet(array $params): array
    {
        return $this->get('/2/tools/aweme_multi_level_category/get/', $params);
    }

    /**
     * 查询抖音类似帐号
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolsAwemeSimilarAuthorSearch(array $params): array
    {
        return $this->get('/2/tools/aweme_similar_author_search/', $params);
    }

    /**
     * 查询抖音帐号和类目信息
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolsAwemeInfoSearch(array $params): array
    {
        return $this->get('/2/tools/aweme_info_search/', $params);
    }

    /**
     * 查询抖音号id对应的达人信息
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolsAwemeAuthorInfoGet(array $params): array
    {
        return $this->get('/2/tools/aweme_author_info/get/', $params);
    }

    /**
     * 查询授权直播抖音达人列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolsLiveAuthorizeList(array $params): array
    {
        return $this->get('/2/tools/live_authorize/list/', $params);
    }

    /**
     * 行为类目查询
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolsInterestActionActionCategory(array $params): array
    {
        return $this->get('/2/tools/interest_action/action/category/', $params);
    }

    /**
     * 行为关键词查询
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolsInterestActionActionKeyword(array $params): array
    {
        return $this->get('/2/tools/interest_action/action/keyword/', $params);
    }

    /**
     * 兴趣类目查询
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolsInterestActionInterestCategory(array $params): array
    {
        return $this->get('/2/tools/interest_action/interest/category/', $params);
    }

    /**
     * 兴趣关键词查询
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolsInterestActionInterestKeyword(array $params): array
    {
        return $this->get('/2/tools/interest_action/interest/keyword/', $params);
    }

    /**
     * 兴趣行为类目关键词id转词
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolsInterestActionId2word(array $params): array
    {
        return $this->get('/2/tools/interest_action/id2word/', $params);
    }

    /**
     * 获取行为兴趣推荐关键词
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolsInterestActionKeywordSuggest(array $params): array
    {
        return $this->get('/2/tools/interest_action/keyword/suggest/', $params);
    }

    /**
     * 查询动态创意词包
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolsCreativeWordSelect(array $params): array
    {
        return $this->get('/2/tools/creative_word/select/', $params);
    }

    /**
     * 查询创编可用人群
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function dmpAudiencesGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/dmp/audiences/get/', $params);
    }

    /**
     * 获取定向包列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function orientationPackageGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/orientation_package/get/', $params);
    }

    /**
     * 获取人群管理列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function audienceListGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/audience_list/get/', $params);
    }

    /**
     * 获取人群分组
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function audienceGroupGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/audience_group/get/', $params);
    }

    /**
     * 上传人群
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function audienceCreateByFile(array $params): array
    {
        return $this->postJson('/v1.0/qianchuan/audience/create_by_file/', $params);
    }

    /**
     * 推送人群
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function audiencePush(array $params): array
    {
        return $this->postJson('/v1.0/qianchuan/audience/push/', $params);
    }

    /**
     * 删除人群
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function audienceDelete(array $params): array
    {
        return $this->postJson('/v1.0/qianchuan/audience/delete/', $params);
    }

    /**
     * 小文件直接上传
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function audienceFileUpload(array $params): array
    {
        return $this->fileUpload('/v1.0/qianchuan/audience_file/upload/', Utils::formParams2Multipart($params));
    }

    /**
     * 大文件分片上传
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function audienceFilePartUpload(array $params): array
    {
        return $this->fileUpload('/v1.0/qianchuan/audience_file/part_upload/', Utils::formParams2Multipart($params));
    }

    /**
     * 设置计划一键起量任务
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolsSmartBoostAdBoostSet(array $params): array
    {
        return $this->postJson('/v1.0/qianchuan/tools/smart_boost/ad_boost/set/', $params);
    }

    /**
     * 获取计划一键起量状态
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolsSmartBoostAdBoostStatusGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/tools/smart_boost/ad_boost/status/get/', $params);
    }

    /**
     * 获取计划一键起量版本
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolsSmartBoostAdBoostVersionGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/tools/smart_boost/ad_boost/version/get/', $params);
    }

    /**
     * 获取计划一键起量报告
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolsSmartBoostAdBoostReportGet(array $params): array
    {
        return $this->get('/v1.0/qianchuan/tools/smart_boost/ad_boost/report/get/', $params);
    }
}
