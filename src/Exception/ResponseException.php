<?php

namespace Peimengc\Crawler\Exception;

use Throwable;

class ResponseException extends Exception
{
    public array $raw = [];

    public function __construct(array $raw, int $code = 0, ?Throwable $previous = null)
    {
        $this->raw = $raw;
        parent::__construct(json_encode($raw), $code, $previous);
    }
}
