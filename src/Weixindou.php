<?php

namespace Peimengc\Crawler;

use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\GuzzleException;
use Peimengc\Crawler\Http\HasHttpClient;

class Weixindou
{
    use HasHttpClient {
        request as preRequest;
    }

    public CookieJar|null $cookieJar = null;

    public string $baseUri = 'https://channels.weixin.qq.com/promote/api/web/';

    public string $format = 'array';

    public function __construct(CookieJar $cookieJar = null)
    {
        $this->cookieJar = $cookieJar;
    }

    /**
     * 发送请求
     *
     * @param string $method
     * @param string $url
     * @param array $options
     * @return array
     * @throws GuzzleException
     */
    public function request(string $method, string $url, array $options = []): array
    {
        $options['cookies'] = $this->cookieJar;
        $options['base_uri'] = rtrim($this->baseUri, '/') . '/';
        $url = ltrim($url, '/');

        return $this->preRequest($method, $url, $options);
    }

    /**
     * 获取登录token
     *
     * @return array
     * @throws GuzzleException
     */
    public function authLoginToken(): array
    {
        return $this->postJson('auth/auth_login_token');
    }

    /**
     * 验证登录token状态
     *
     * @param $token
     * @return array
     * @throws GuzzleException
     */
    public function authLoginStatus($token): array
    {
        return $this->postJson('auth/auth_login_status', ['token' => $token]);
    }

    /**
     * 用户信息
     *
     * @return array
     * @throws GuzzleException
     */
    public function userPrepare(): array
    {
        return $this->postJson('auth/user_prepare');
    }

    /**
     * 短视频订单列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function selectFeedPromotion(array $params): array
    {
        return $this->postJson('promotion/select-feed-promotion', $params);
    }

    /**
     * 短视频订单详情
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function getFeedPromotionOrderDetail(array $params): array
    {
        return $this->postJson('promotion/get-feed-promotion-order-detail', $params);
    }

    /**
     * 短视频订单停止加热
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function cancelFeedPromotion(array $params): array
    {
        return $this->postJson('promotion/cancel-feed-promotion', $params);
    }

    /**
     * 直播订单列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function searchLivePromotionOrderList(array $params): array
    {
        return $this->postJson('promotion-live/search-live-promotion-order-list', $params);
    }

    /**
     * 搜索被投号/搜索相似达人
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function searchAccount(array $params): array
    {
        return $this->postJson('promotion/search-account', $params);
    }

    /**
     * 搜索被投号视频
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function getFinderObject(array $params): array
    {
        return $this->postJson('promotion/get-finder-object', $params);
    }

    /**
     * 被投号信息
     *
     * @param array $params ['finderUsername'=>'','needFeatureSwitch'=>true]
     * @return array
     * @throws GuzzleException
     */
    public function getFinderUserAttr(array $params): array
    {
        return $this->postJson('promotion-live/get-finder-user-attr', $params);
    }

    /**
     * 被投号是否可以投放
     *
     * @param array $params ['finderUsername'=>'']
     * @return array
     * @throws GuzzleException
     */
    public function getLivePromotionRelations(array $params): array
    {
        return $this->postJson('promotion-live/get-live-promotion-relations', $params);
    }

    /**
     * 直播出价信息
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function getLivePromotionPriceInfo(array $params = []): array
    {
        return $this->postJson('promotion-live/get-live-promotion-price-info', $params);
    }

    /**
     * 直播信息
     *
     * @param array $params ['finderUsername'=>'']
     * @return array
     * @throws GuzzleException
     */
    public function getFinderLiveInfo(array $params): array
    {
        return $this->postJson('promotion-live/get-finder-live-info', $params);
    }

    /**
     * 常用被投号列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function getLivePromotionAccountUseList(array $params = []): array
    {
        return $this->postJson('promotion-live/get-live-promotion-account-use-list', $params);
    }

    /**
     * 创建直播订单
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function bootLivePromotion(array $params = []): array
    {
        return $this->postJson('promotion-live/book-live-promotion', $params);
    }

    /**
     * 直播订单详情
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function getLivePromotionOrderDetail(array $params = []): array
    {
        return $this->postJson('promotion-live/get-live-promotion-order-detail', $params);
    }

    /**
     * 投放号信息
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function getLivePromotionUserProfile(array $params = []): array
    {
        return $this->postJson('promotion-live/get-live-promotion-user-profile', $params);
    }

    /**
     * 取消直播订单
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function cancelLivePromotion(array $params = []): array
    {
        return $this->postJson('promotion-live/cancel-live-promotion', $params);
    }
}
