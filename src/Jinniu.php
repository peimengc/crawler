<?php

namespace Peimengc\Crawler;

use GuzzleHttp\Exception\GuzzleException;
use Peimengc\Crawler\Exception\ResponseException;
use Peimengc\Crawler\Http\HasHttpClient;

class Jinniu
{
    use HasHttpClient {
        request as preRequest;
    }

    public string $accessToken = '';

    public string $baseUri = 'https://ad.e.kuaishou.com';

    public string $appId;

    public string $secret;

    public string $format = 'array';

    public function __construct($app_id, $secret)
    {
        $this->appId = $app_id;
        $this->secret = $secret;
    }

    /**
     * 发送请求
     *
     * @param string $method
     * @param string $url
     * @param array $options
     * @return array
     * @throws GuzzleException
     * @throws ResponseException
     */
    public function request(string $method, string $url, array $options = []): array
    {
        $options['headers']['Access-Token'] = $this->accessToken;
        $options['base_uri'] = $this->baseUri;

        $result = $this->preRequest($method, $url, $options);

        if ($result['code'] !== 0) {
            throw new ResponseException($result);
        }

        return $result;
    }

    /**
     * 获取token
     *
     * @param $code
     * @return array
     * @throws GuzzleException
     */
    public function oauth2AccessToken($code): array
    {
        $result = $this->postJson('/rest/openapi/oauth2/authorize/access_token', [
            'app_id' => $this->appId,
            'secret' => $this->secret,
            'auth_code' => $code
        ]);

        $this->accessToken = $result['data']['access_token'];

        return $result;
    }

    /**
     * 刷新token
     *
     * @param $refreshToken
     * @return array
     * @throws GuzzleException
     */
    public function oauth2RefreshToken($refreshToken): array
    {
        $result = $this->postJson('/rest/openapi/oauth2/authorize/refresh_token', [
            'app_id' => $this->appId,
            'secret' => $this->secret,
            'refresh_token' => $refreshToken
        ]);

        $this->accessToken = $result['data']['access_token'];

        return $result;
    }

    /**
     * 广告列表查询
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function reportAdSearch(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/report/ad/search',
            $params,
        );
    }

    /**
     * 创建广告计划
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function campaignCreate(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/campaign/create',
            $params,
        );
    }

    /**
     * 修改广告计划
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function campaignUpdate(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/campaign/update',
            $params,
        );
    }

    /**
     * 修改广告计划状态
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function campaignUpdateStatus(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/v1/campaign/update/status',
            $params,
        );
    }

    /**
     * 创建广告组
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function unitCreate(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/unit/create',
            $params,
        );
    }

    /**
     * 修改广告组
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function unitUpdate(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/unit/update',
            $params,
        );
    }

    /**
     * 修改广告组状态
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function unitUpdateStatus(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/unit/update/status',
            $params,
        );
    }

    /**
     * 修改广告组预算
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function unitUpdateBudget(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/unit/update/budget',
            $params,
        );
    }

    /**
     * 修改广告组出价
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function unitUpdateCpaBid(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/unit/update/cpaBid',
            $params,
        );
    }

    /**
     * 修改广告组roi
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function unitUpdateRoi(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/unit/update/roi',
            $params,
        );
    }

    /**
     * 创建创意
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function creativeCreate(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/creative/create',
            $params,
        );
    }

    /**
     * 修改创意
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function creativeUpdate(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/creative/update',
            $params,
        );
    }

    /**
     * 修改创意状态
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function creativeUpdateStatus(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/creative/update/status',
            $params,
        );
    }

    /**
     * 获取可选的推荐封面
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function creativeHelperKeyFrame(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/creative/helper/keyFrame',
            $params,
        );
    }

    /**
     * 获取可选的动态词包
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function creativeHelperListBagWords(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/creative/helper/list/bagWords',
            $params,
        );
    }

    /**
     * 获取行动号召按
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function creativeHelperListActionBarText(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/creative/helper/list/actionBarText',
            $params,
        );
    }

    /**
     * 获取推荐理由
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function creativeWordList(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/v1/tool/ecom/creativeWord/list',
            $params,
        );
    }

    /**
     * 创建程序化 2.0 创意
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function creativeAdvancedProgramCreate(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/creative/advanced/program/create',
            $params,
        );
    }

    /**
     * 编辑程序化 2.0 创意
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function creativeAdvancedProgramUpdate(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/creative/advanced/program/update',
            $params,
        );
    }

    /**
     * 获取地域信息
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolRegionList(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/tool/region/list',
            $params,
        );
    }

    /**
     * 获取购物意图信息
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolPurchaseIntentionList(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/tool/purchase_intention/list',
            $params,
        );
    }

    /**
     * 获取网红信息-网红查询
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolCelebritySearch(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/tool/celebrity/search',
            $params,
        );
    }

    /**
     * 获取网红信息-网红分类
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function toolCelebrityList(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/tool/celebrity/list',
            $params,
        );
    }

    /**
     * 上传视频
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function filemanageVideoUpload(array $params): array
    {
        return $this->fileUpload(
            '/rest/openapi/gw/esp/v1/filemanage/video/upload',
            Utils::formParams2Multipart($params)
        );
    }

    /**
     * 查询视频库
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function filemanageVideoList(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/filemanage/video/list',
            $params
        );
    }

    /**
     * 视频标签查询
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function filemanageVideoTagList(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/filemanage/video/tag/list',
            $params
        );
    }

    /**
     * 查询视频信息
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function filemanageVideoInfoQuery(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/filemanage/video/info/query',
            $params
        );
    }

    /**
     * 上传图片
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function filemanagePicUpload(array $params): array
    {
        return $this->fileUpload(
            '/rest/openapi/gw/esp/v1/filemanage/pic/upload',
            Utils::formParams2Multipart($params)
        );
    }

    /**
     * 图片列表查询
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function filemanagePicList(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/filemanage/pic/list',
            $params
        );
    }

    /**
     * 自定义列查询
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function reportMetaData(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/report/meta_data',
            $params
        );
    }

    /**
     * 报表查询
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function reportList(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/report/list',
            $params
        );
    }

    /**
     * 账户流水
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function accountFinanceFlowList(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/account/finance/flow/list',
            $params
        );
    }

    /**
     * 查询账户资质
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function accountQualificationInfo(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/account/qualification/info',
            $params
        );
    }

    /**
     * 查询账户余额
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function accountBalanceInfo(array $params): array
    {
        return $this->postJson(
            '/rest/openapi/gw/esp/v1/account/balance/info',
            $params
        );
    }
}
