<?php

namespace Peimengc\Crawler\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\RequestInterface;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\Middleware;

trait HasHttpClient
{
    /**
     * @var Client|null
     * @see https://guzzle-cn.readthedocs.io/zh_CN/latest/overview.html
     */
    public Client|null $httpClient = null;

    /**
     * Client 配置
     * @var array
     * @see https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html
     */
    public static array $clientOptions = [];

    protected bool $debug = false;

    /**
     * 获取请求类
     *
     * @return Client
     */
    public function getHttpClient(): Client
    {
        if ($this->httpClient === null) {
            $config = self::$clientOptions;
            if ($this->debug) {
                $config['handler'] = $this->handler();
            }
            $this->httpClient = new Client($config);
        }
        return $this->httpClient;
    }

    public function debug($debug = true)
    {
        $this->debug = $debug;
        return $this;
    }

    public function handler()
    {
        $stack = new HandlerStack();
        $stack->setHandler(new CurlHandler());

        $stack->push(Middleware::mapRequest(function (RequestInterface $request) {

            print_r([
                'url' => (string)$request->getUri(),
                'method' => $request->getMethod(),
                'body' => $request->getBody()->getContents(),
                'headers' => $request->getHeaders(),
            ]);

            return $request;
        }));

        $stack->push(Middleware::mapResponse(function (ResponseInterface $response) {

            print_r([
                'status_code' => $response->getStatusCode(),
                'body' => $response->getBody()->getContents(),
                'headers' => json_encode($response->getHeaders(), JSON_UNESCAPED_UNICODE)
            ]);

            return $response;
        }));

        return $stack;
    }

    /**
     * 发送请求
     *
     * @param string $method
     * @param string $url
     * @param array $options
     * @return string|array|ResponseInterface
     * @throws GuzzleException
     */
    public function request(string $method, string $url, array $options = []): string|array|ResponseInterface
    {
        $response = $this->getHttpClient()->request($method, $url, $options);
        $response->getBody()->rewind();
        return $this->unwrapResponse($response);
    }

    /**
     * 格式化内容
     *
     * @param ResponseInterface $response
     * @return string|array|ResponseInterface
     */
    public function unwrapResponse(ResponseInterface $response): string|array|ResponseInterface
    {
        if (!property_exists($this, 'format')) {
            return $response;
        }

        if ($this->format == 'array') {
            return json_decode($response->getBody()->getContents(), true) ?? [];
        }

        if ($this->format === 'raw') {
            return $response->getBody()->getContents();
        }


        return $response;
    }

    /**
     * 发送GET请求
     *
     * @param $url
     * @param array|null $query
     * @return string|array|ResponseInterface
     * @throws GuzzleException
     */
    public function get($url, array $query = null): string|array|ResponseInterface
    {
        return $this->request('GET', $url, [
            'query' => $query
        ]);
    }

    /**
     * 发送POST请求
     *
     * @param $url
     * @param array $formParams
     * @param array $query
     * @return string|array|ResponseInterface
     * @throws GuzzleException
     */
    public function post($url, array $formParams = [], array $query = []): string|array|ResponseInterface
    {
        return $this->request('POST', $url, [
            'query' => $query,
            'form_params' => $formParams
        ]);
    }

    /**
     * 发送POST JSON请求
     *
     * @param $url
     * @param array $json
     * @param array $query
     * @return string|array|ResponseInterface
     * @throws GuzzleException
     */
    public function postJson($url, array $json = [], array $query = []): string|array|ResponseInterface
    {
        return $this->request('POST', $url, [
            'query' => $query,
            'json' => $json
        ]);
    }

    /**
     * 文件上传
     *
     * @param $url
     * @param array $multipart
     * @return string|array|ResponseInterface
     * @throws GuzzleException
     */
    public function fileUpload($url, array $multipart): string|array|ResponseInterface
    {
        return $this->request('POST', $url, [
            'multipart' => $multipart
        ]);
    }
}
