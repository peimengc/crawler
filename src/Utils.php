<?php

namespace Peimengc\Crawler;

class Utils
{
    public static function formParams2Multipart(array $params)
    {
        $multipart = [];
        foreach ($params as $k => $v) {
            $multipart[] = [
                'name' => $k,
                'contents' => $v
            ];
        }
        return $multipart;
    }

    public static function getUrlQuery($uri, $key = null)
    {
        parse_str(parse_url($uri)['query'] ?? '', $query);

        if ($key) {
            return $query[$key] ?? null;
        }

        return $query;

    }
}